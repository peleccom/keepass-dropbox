#!/usr/bin/env python
# -*- coding: UTF-8 -*-


# File name (without path) in which to store the access keys to not ask for permission each time
# имя файла (без пути) в котором будут сохранены ключи доступа чтобы не запрашивать разрешение каждый раз
CACHED_ACCESS_FILE = u'cached.txt' 

# local file path
# имя файла на локальном компьютере
CHECKING_FILE = ur"path\\to\\file.kdbx"

# filepath at dropbox
# имя файла на dropbox
CHECKING_FILE_REMOTE_NAME=ur"file_name_at_dropbox.kdbx"

# your time zone
# ваш часовой пояс
# see http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
localtzname = "Europe/Minsk" 

# Get your app key and secret from the Dropbox developer website
# Сюда вводим Appkey и AppSecret приложения с "https://www.dropbox.com/developers/apps"
APP_KEY = ''
APP_SECRET = ''

# ACCESS_TYPE should be 'dropbox' or 'app_folder' as configured for your app
# Оставляем это значение
ACCESS_TYPE = 'dropbox'