﻿Read English Version english_.
================================
Синхронизация KeePass_ с DropBox
================================

Как это работает
================
Плагин KPDataSave_ умеет только сохранять файлы на DropBox, этот скрипт скачивает новую версию 
файла с DropBox если дата изменения фала на сервере больше чем файла на локальном компьютере.
Настройки хранятся в файле *keepass_config.py* . Предварительно надо зарегистрировать новое приложение 
по адресу https://www.dropbox.com/developers/apps и дать ему полный доступ к DropBox полученные значения 
key и secret сохранить в файл.
Запускать файл *keepass.py* **до старта** самого keepass. Для этого можно использовать 
триггеры в keepass (Сервис-Триггеры).

Зависимости
===========

* pytz
* dropbox

Автоматическая установка зависимостей

>> ``pip install -r requirements.txt``

------------

.. _english:

===========================
Sync KeePass_ with DropBox
===========================

How it works
============
Plugin KPDataSave_ can only save files in the DropBox, the script will download the new version
DropBox file if the date changes halyard on the server more than a file on your computer.
Settings are stored in the file *keepass_config.py*. Pre-need to register a new application
at https://www.dropbox.com/developers/apps and give it a full access to the DropBox
secret key and save the file.
Run the file *keepass.py* **prior to the start** of the keepass. You can use the
triggers in the keepass (service-triggers).

Dependencies
============

* pytz 
* dropbox

Automatic installation of requirements

>> ``pip install -r requirements.txt``

.. _KPDataSave: https://bitbucket.org/HexRx/kpdatasave
.. _KeePass: http://keepass.info/