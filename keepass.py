#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Плагин DropBoxSave умеет только сохранять файлы на drpopbox
# Этот скрипт должен запускать до открытия файла с паролями (например через 
# триггеры в keepass) 
# он проверяет не загружена ли более новая версия фвйла на 
# dropbox и в случае необходимости скачивает его
# для работы надо зарегистрировать 
# новое приложение на "https://www.dropbox.com/developers/apps" и 
# сконфигурировать скрипт полученными даннымм

# Include the Dropbox SDK libraries
import dropbox
from dropbox.rest import ErrorResponse
import webbrowser
import os
import sys
import datetime
from dateutil.parser import parse as dateparse
import pytz
import logging
from keepass_config import *

localtz = pytz.timezone(localtzname)

BASE_PATH = os.path.abspath(os.path.dirname(__file__))

logging.basicConfig(filename=os.path.join(BASE_PATH, 'dropboxsync.log'),
    level=logging.DEBUG, format="%(levelname)s:%(asctime)s:%(message)s")
CACHED_ACCESS_FILE = os.path.join(BASE_PATH, CACHED_ACCESS_FILE)

sess = dropbox.session.DropboxSession(APP_KEY, APP_SECRET, ACCESS_TYPE)
need_auth = True


if os.path.exists(CACHED_ACCESS_FILE):
    try:
        need_auth = False
        # we have saved acees token
        with open(CACHED_ACCESS_FILE) as f:
            token, secret = f.read().split()

        sess.set_token(token, secret)
        client = dropbox.client.DropboxClient(sess)
        client.account_info()
    except Exception, e:
        need_auth = True


if need_auth:

    # Get request token
    request_token = sess.obtain_request_token()

    url = sess.build_authorize_url(request_token)
    webbrowser.open(url)
    print("url:", url)
    print ("Please visit this website and press the 'Allow' button, then hit 'Enter' here.")
    raw_input()

    # This will fail if the user didn't visit the above URL and hit 'Allow'
    access_token = sess.obtain_access_token(request_token)

    # save to file
    with open(CACHED_ACCESS_FILE, 'w') as f:
        f.write(' '.join([access_token.key, access_token.secret]))
    client = dropbox.client.DropboxClient(sess)

#print "linked account:", client.account_info()
try:
    metadata = client.metadata(CHECKING_FILE_REMOTE_NAME)
except ErrorResponse as e:
    logging.error("Error %s", e)
    sys.exit(1)
if metadata.get("is_deleted"):
    logging.error("File deleted")
    sys.exit(1)
dpdate = metadata['modified']

dpdate = dateparse(dpdate) 
print("Server file time %s" % dpdate)


# check local file
ldate = localtz.localize(datetime.datetime.fromtimestamp(os.path.getmtime(CHECKING_FILE)))
print("Local file time %s" % ldate)


if (dpdate > ldate):
    print("Delta is %s" % (dpdate - ldate))
    if (dpdate - ldate) > datetime.timedelta(minutes=10):
        print("Updated")

        logging.log(logging.INFO, "Server has newest version updated at %s local file updated at %s . Updating" % 
                (dpdate, ldate))
        # downloading a file
        try:
            response = client.get_file(CHECKING_FILE_REMOTE_NAME)
            open(CHECKING_FILE + ".bak", "wb").write(open(CHECKING_FILE, 'rb').read())
            open(CHECKING_FILE, "wb").write(response.read())
            logging.log(logging.INFO, "Saved")
            print("New file saved")
        except Exception, e:
            logging.log(logging.ERROR, "An error while downloading new file %s",
                e)
    else:
        print("Not updated")
        logging.log(logging.INFO, "Not updated")
else:
    print("Local file has newer date. Delta is %s" % (ldate - dpdate))
    logging.log(logging.INFO, "Not updated")
